﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using System.Web.UI;
using SelectPdf;
using System.Globalization;

public partial class CurrentTaskToPdf : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e){
        
    }

    protected void btn_click(object sender, EventArgs e)
    {
        var textBoxValue = ASPxTextBox.Value;
        try
        {
            if (textBoxValue != null)
            {
                using (TaskToPdfEntities1 entity = new TaskToPdfEntities1())
                {
                    var data = entity.GetCurrentTasks(Convert.ToInt32(textBoxValue));
                    var stringData = JsonConvert.SerializeObject(data);
                    var transformedData = JsonConvert.DeserializeObject<GetCurrentTasks_Result[]>(stringData);
                    GeneratePdf(transformedData, textBoxValue.ToString());
                }
            }
        }
        catch(Exception ex)
        {
            throw ex;
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "hideLoader();", true);
    }

    public void GeneratePdf(GetCurrentTasks_Result[] data, string reportId)
    {
        StringBuilder sb = new StringBuilder();
        string reportHtmlPath = Server.MapPath("~/wwwroot/Pdf/");
        if (!Directory.Exists(reportHtmlPath))
            Directory.CreateDirectory(reportHtmlPath);

        string fileName = "report" + reportId;
        string path = reportHtmlPath + fileName + ".html";
        using (StreamWriter sw = System.IO.File.CreateText(path))
        {
            sb.Append("<!doctype html><html lang='en'><head><meta charset='UTF-8'><meta name='viewport' content='width = device-width, initial - scale = 1.0'>");
            sb.Append("<meta http-equiv='X-UA-Compatible' content='ie=edge'><title>Report</title>");
            sb.Append("<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity='sha384 - ggOyR0iXCbMQv3Xipma34MD + dH / 1fQ784 / j6cY / iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'>'");
            sb.Append("<style>.center-align{text-align:center} .head {text-align: center} td{font-size:9px;font-family: sans-serif;}th{font-size:9px;font-family: sans-serif;} .table td, .table th {padding: 0.03rem !important} .bold{font-weight:700} .check {border: 1px solid black;text-align: center; } .bold { font-weight: 700; font-size: 12px;} .duration {display:flex; align-items: flex-end; font-size: 12px; } .font-6 {font-size:6.5px;} .font-8{font-size:8px;} </style>");
            sb.Append("</head><body>");

            sb.Append("<div style='padding: 0 20px 0px 20px'><div class='row'><div class='col-md-10 head'> <span class='bold'> TAMAWOOD BUILDING CONSTRUCTION PROJECT -ALL JOBS</span><br/><span class='bold'>Warning: Data on your performance against booked  dates is monitored for future work allocations</span></div><div class='col-md-2 duration'><span>" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "</span></div></div>");
            sb.Append("<div><table class='table' ><thead><tr><th class='center-align font-8' style='width: 22px;'>----- <br/>Pri</th><th class='center-align font-8' style='width:36px'>Days <br/>OnList</th><th style='width:40px' class='font-8'>-----<br />Alloc</th><th class='font-8'>BuilderName</th> <th class='font-8'>ClientName</th><th class='font-8'>LotAddress</th><th class='font-8' style='width: 34px;'>Region</th><th class='font-8' style='width: 56px;'>PowerOnSite</th><th style='width: 35px;' class='font-8'>Due Date</th><th class='font-8' style='width: 82px;'>Booked For(dd/mm)</th><th class='font-8' style='width: 82px;'>Booked On(dd/mm)</th><th class='font-8'>PO No.</th><th class='font-8'>Completed</th></tr></thead><tbody>");

            string previousUd = "", previousTN = "";
            foreach (var item in data)
            {
                var ud = item.UrgencyDescription;
                var tn = item.TaskName;

                if (previousUd != ud)
                {
                    sb.Append("<tr style='background-color: #D3D3D3;font-size: 9px;' class='bold'><td colspan='15'>" + ud + "</td></tr>");
                    previousUd = ud;
                    previousTN = "";
                }

                if (previousTN != tn)
                {
                    sb.Append("<tr ><td colspan='13' class='bold'><div style='background-color: #e8e6e6; ;font-size: 9px;margin-left: 6px;'>" + tn + "</div></td></tr>");
                    previousTN = tn;
                }

                sb.Append("<tr>");
                sb.Append("<td style='text-align:right' class='font-6'>" + item.JobPriority + "</td>");
                sb.Append("<td style='text-align:center' class='font-6'>" + item.DaysOnList + "</td>");
                sb.Append("<td class='font-6'>" + item.DaysAssigned + "</td>");
                sb.Append("<td class='font-6'>" + item.BuilderName + "</td>");
                sb.Append("<td class='font-6'>" + item.ClientName + "</td>");
                sb.Append("<td class='font-6'>" + item.LotAddress + "</td>");
                sb.Append("<td class='font-6'>" + item.Region + "</td>");
                if (item.Power == 1)
                {
                    sb.Append("<td><input type='checkbox' checked /></td>");
                }
                else
                {
                    sb.Append("<td><input type='checkbox' /></td>");
                }
               
                sb.Append("<td class='font-6'>" + item.DueDate.ToString("dd/MM", CultureInfo.InvariantCulture) + "</td>");
                sb.Append("<td class='font-6'>" + item.BookedFor.Value.ToString("dd/MM",  CultureInfo.InvariantCulture) + "</td>");
                sb.Append("<td class='font-6'>" + item.BookedOn.Value.ToString("dd/MM", CultureInfo.InvariantCulture) + "</td>");
                sb.Append("<td class='font-6'>" + item.ponum + "</td>");
                sb.Append("<td style='text-align:center'><input type='checkbox'/></td>");
                sb.Append("</tr>");
            }

            sb.Append("</tbody></table></div></div>");
            sw.WriteLine(sb);
        }
        string fileNamePdf = fileName + ".pdf";
        string pdfFilePath = reportHtmlPath + fileNamePdf;
        SelectPdf.HtmlToPdf converter = new SelectPdf.HtmlToPdf();



        //First header
        converter.Options.DisplayHeader = true;
        converter.Header.DisplayOnFirstPage = true;
        converter.Header.DisplayOnOddPages = true;
        converter.Header.DisplayOnEvenPages = true;
        converter.Header.Height = 50;
        PdfHtmlSection header = new PdfHtmlSection("<!DOCTYPE html><html lang='en'><head><meta charset='utf-8' /><title></title><link rel='stylesheet' href= 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity = 'sha384 - ggOyR0iXCbMQv3Xipma34MD + dH / 1fQ784 / j6cY / iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin = 'anonymous' ><style>.bold {font-weight: 700;}.text-font {font-size: 12px;}</style></head><body><div class='row text-font' style='margin-left: 30px; margin-right: 30px; padding: 0; margin-top: 20px'><div class='col-md-3'><div style ='display:flex;'><div>Attention:</div><div class='bold'>" + data[0].scont + "<br />" + data[0].snam + "</div></div><div>Fax No.: <span class='bold'>07 3807 4664</span></div></div><div class='col-md-3'></div><div class='col-md-4'><span class='bold'>CONFIDENTIAL - For Internal Use Only<br /> Dixonbuild Pty Ltd ABN 80 112 083 842 <br /> Ph: 1300 10 10 10</span></div><div class='col-md-2'><img src=" + Server.MapPath("~/wwwroot/DixonHomes.png") + " style='width: 100%' /></div></div></body></html> ", "~/wwwroot/Header1.html");
        header.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
        converter.Header.Add(header);

        // footer settings
        converter.Options.DisplayFooter = true;
        converter.Footer.DisplayOnFirstPage = true;
        converter.Footer.DisplayOnOddPages = true;
        converter.Footer.DisplayOnEvenPages = true;
        converter.Footer.Height = 50;


        // add some html content to the footer
        PdfHtmlSection footerHtml = new PdfHtmlSection("<!DOCTYPE html><html><head><meta charset='utf-8' /><title></title><link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity='sha384-ggOyR0iXCbMQv3Xipma34MD + dH / 1fQ784 / j6cY / iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'></head><body><footer style='margin-left: 30px; margin-right: 30px;margin-bottom: 10px; font-size:12px;'><hr style='border-top: 2px solid black; margin-bottom: 0 !important;'><div class='row' style='font-size: 13px;'><div class='col-md-2'></div><div class='col-md-2'>" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "</div><div class='col-md-1'></div><div class='col-md-7' style='text-align:right'>Tick tasks done and Fax back to Production Co-ordinators on(07) 3274 0794 (Before 6PM)</div></div></footer></body></html>", "~/wwwroot/Footer.html");
        footerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
        footerHtml.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
        converter.Footer.Add(footerHtml);

        // page numbers can be added using a PdfTextSection object
        PdfTextSection text = new PdfTextSection(22, 13, "Page {page_number} of {total_pages}", new System.Drawing.Font("Arial", 8));
        text.HorizontalAlign = PdfTextHorizontalAlign.Left;
        converter.Footer.Add(text);

        SelectPdf.PdfDocument doc = converter.ConvertUrl(path);
        doc.Save(pdfFilePath);
        doc.Close();
        var res = ConvertPdfToByte(pdfFilePath);
        ucWebUserControl.PdfData = res;
    }

    public byte[] ConvertPdfToByte(string pdfFilePath)
    {
        byte[] buffer;
        using (Stream stream = new System.IO.FileStream(pdfFilePath, FileMode.Open))
        {
            buffer = new byte[stream.Length - 1];
            stream.Read(buffer, 0, buffer.Length);
        }
        return buffer;
    }
}