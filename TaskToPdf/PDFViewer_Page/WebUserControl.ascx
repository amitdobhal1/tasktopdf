﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebUserControl.ascx.cs" Inherits="PDFViewer_Page_WebUserControl" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v21.1, Version=21.1.5.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v21.1, Version=21.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<style>
    .viewerPanel {
        display: flex;
        flex-direction: column;
        text-align: center;
        align-items: center;
    }

    .viewerPanel {
        height: calc(100vh - 100px);
        overflow: scroll;
    }

    .floatLeft {
        float: left;
        padding-left: 5px;
    }

    .floatRight {
        float: right;
        padding-right: 5px;
    }

    .top4 {
        padding-top: 4px;
    }

    .PopupHeader {
        height: 50px;
    }

    .findButton {
        height: 20px;
        float: left;
    }

    .roundedCorner {
        border-radius: 5px;
    }
</style>
<script>

    printPreview = (byteArray, contentType = 'application/pdf') => {
        const blob = new Blob([new Uint8Array(byteArray)], { type: contentType });
        const blobURL = URL.createObjectURL(blob);
        const printWindow = window.open(blobURL);
        const thisDoc = printWindow.document;
        const printScript = document.createElement('script');
        function printPDF() {
            window.print();
        }
        printScript.innerHTML = `window.onload = ${printPDF.toString()};`;
        thisDoc.body.appendChild(printScript);
    };

    function OnRibbonCommandExecuted(s, e) {
        var currentPage = ASPxSpinEditCurrentPage.GetValue();
        if (e.item.name == "Previous") {
            if (currentPage > 1) currentPage--;
        }
        else if (e.item.name == "Next") {
            if (currentPage < ASPxLabelTotalPage.GetText())
                currentPage++;
        } else if (e.item.name == "Email") {
            ASPxPopupControlEmail.Show();
            return;
        }
        else if (e.item.name == "SaveAs") {
            ASPxButtonDownload.DoClick();
            return;
        }
        else if (e.item.name == "Zoom") { return; }
        ASPxSpinEditCurrentPage.SetValue(currentPage);
        cbpViewer.PerformCallback(e.item.name);
    }

    function ASPxSpinEditCurrentPage_KeyDown(s, e) {
        if (e.htmlEvent.keyCode == 13) {
            cbpViewer.PerformCallback("Pages");
            e.htmlEvent.preventDefault();
        }
    }

    function SearhButtonHeader_KeyDown(s, e) {
        if (e.htmlEvent.keyCode == 13) {
            cbpViewer.PerformCallback("Find");
            e.htmlEvent.preventDefault();
        }
    }

    function SearhButtonHeader_ButtonClick(s, e) {
        if (e.buttonIndex == 1)
            cbpViewer.PerformCallback("Find-1");
        else if (e.buttonIndex == 2)
            cbpViewer.PerformCallback("Find+1");
        else
            cbpViewer.PerformCallback("Find");
    }

    function SearhButtonHeader_LostFocus() {
        let fText = cSearhButton.GetText();
        if (fText == "")
            cbpViewer.PerformCallback();
    }

    function cbpViewer_EndCallback(s, e) {
        if (s.cp_PageNumber)
            ASPxSpinEditCurrentPage.SetValue(s.cp_PageNumber);
        if (s.cp_PDFData) {
            printPreview(s.cp_PDFData);
            delete (s.cp_PDFData);
        }
        if (s.cp_MailStatus) {
            if (s.cp_MailStatus == "SUCCESS") {
                alert("Mail queued successfuly.");
                ASPxPopupControlEmail.Hide();
            }
            else
                alert(s.cp_MailStatus);
            delete (s.cp_MailStatus);
        }
    }

    function ASPxButtonSendEmail_Click() {
        cbpViewer.PerformCallback("Email");
    }

</script>
<div id="pdfWindow" runat="server" style="display: none;">
<dx:ASPxButton ID="ASPxButtonDownload" ClientInstanceName="ASPxButtonDownload" runat="server" OnClick="ASPxButtonDownload_Click" ClientVisible="false"></dx:ASPxButton>

<dx:ASPxRibbon ID="ASPxRibbonDocument" runat="server" ClientInstanceName="ASPxRibbonDocument" ShowFileTab="false" ShowTabs="false">
    <ClientSideEvents CommandExecuted="OnRibbonCommandExecuted" />
    <Tabs>
        <dx:RibbonTab Name="Main" Text="">
            <Groups>
                <dx:RibbonGroup Name="File" Text="File" ShowDialogBoxLauncher="false">
                    <Items>
                        <dx:RibbonButtonItem Name="SaveAs" Size="Large" Text="Save As">
                            <LargeImage IconID="save_saveas_32x32">
                            </LargeImage>
                        </dx:RibbonButtonItem>
                        <dx:RibbonButtonItem Name="Print" Size="Large" Text="Print">
                            <LargeImage IconID="print_print_32x32">
                            </LargeImage>
                        </dx:RibbonButtonItem>
                        <dx:RibbonButtonItem Name="Email" Size="Large" Text="Email">
                            <LargeImage IconID="mail_mail_32x32">
                            </LargeImage>
                        </dx:RibbonButtonItem>
                    </Items>
                </dx:RibbonGroup>
                <dx:RibbonGroup Name="Navigation" Text="Navigation" ShowDialogBoxLauncher="false">
                    <Items>
                        <dx:RibbonButtonItem Name="Previous" Text="Previous" Size="Large">
                            <LargeImage IconID="navigation_previous_32x32">
                            </LargeImage>
                        </dx:RibbonButtonItem>
                        <dx:RibbonButtonItem Name="Next" Text="Next" Size="Large">
                            <LargeImage IconID="navigation_next_32x32">
                            </LargeImage>
                        </dx:RibbonButtonItem>
                        <dx:RibbonTemplateItem Name="Pages" Size="Large">
                            <ItemStyle VerticalAlign="Middle" Width="100px" HorizontalAlign="Center" />
                            <Template>
                                <br />
                                <dx:ASPxSpinEdit ID="ASPxSpinEditCurrentPage" ClientInstanceName="ASPxSpinEditCurrentPage" runat="server" Width="50px" CssClass="floatLeft" MinValue="1" MaxValue="999999999" NumberType="Integer" SpinButtons-ClientVisible="false">
                                    <ClientSideEvents KeyDown="ASPxSpinEditCurrentPage_KeyDown" />
                                </dx:ASPxSpinEdit>
                                <dx:ASPxLabel ID="ASPxLabelOf" runat="server" Text=" of " CssClass="floatLeft top4"></dx:ASPxLabel>
                                <dx:ASPxLabel ID="ASPxLabelTotalPage" ClientInstanceName="ASPxLabelTotalPage" runat="server" CssClass="floatLeft top4"></dx:ASPxLabel>
                            </Template>
                        </dx:RibbonTemplateItem>
                        <dx:RibbonTemplateItem Name="Find" Size="Large">
                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                            <Template>
                                <br />
                                <dx:ASPxButtonEdit ID="SearhButtonHeader" runat="server" ClientInstanceName="cSearhButton" NullText="Search ..."
                                    Width="200px">
                                    <ClearButton DisplayMode="OnHover"></ClearButton>
                                    <Buttons>
                                        <dx:EditButton Image-IconID="find_find_16x16"></dx:EditButton>
                                        <dx:EditButton Image-IconID="arrows_prev_16x16"></dx:EditButton>
                                        <dx:EditButton Image-IconID="arrows_next_16x16"></dx:EditButton>
                                    </Buttons>
                                    <ClientSideEvents KeyDown="SearhButtonHeader_KeyDown" ButtonClick="SearhButtonHeader_ButtonClick" LostFocus="SearhButtonHeader_LostFocus" />
                                </dx:ASPxButtonEdit>
                            </Template>
                        </dx:RibbonTemplateItem>
                    </Items>
                </dx:RibbonGroup>
                <dx:RibbonGroup Name="Zoom" Text="Zoom" ShowDialogBoxLauncher="false">
                    <Items>

                        <dx:RibbonButtonItem Size="Large" Name="ZoomIn" Text="Zoom In">
                            <LargeImage IconID="zoom_zoomin_32x32">
                            </LargeImage>
                        </dx:RibbonButtonItem>
                        <dx:RibbonButtonItem Size="Large" Name="ZoomOut" Text="Zoom Out">
                            <LargeImage IconID="zoom_zoomout_32x32">
                            </LargeImage>
                        </dx:RibbonButtonItem>
                        <dx:RibbonDropDownButtonItem Name="Zoom" Size="Large" Text="Zoom">
                            <LargeImage IconID="zoom_zoom_32x32">
                            </LargeImage>
                            <Items>
                                <dx:RibbonDropDownButtonItem Name="Zoom10" Text="10%"></dx:RibbonDropDownButtonItem>
                                <dx:RibbonDropDownButtonItem Name="Zoom25" Text="25%"></dx:RibbonDropDownButtonItem>
                                <dx:RibbonDropDownButtonItem Name="Zoom50" Text="50%"></dx:RibbonDropDownButtonItem>
                                <dx:RibbonDropDownButtonItem Name="Zoom100" Text="100%"></dx:RibbonDropDownButtonItem>
                                <dx:RibbonDropDownButtonItem Name="Zoom150" Text="150%"></dx:RibbonDropDownButtonItem>
                                <dx:RibbonDropDownButtonItem Name="Zoom200" Text="200%"></dx:RibbonDropDownButtonItem>
                                <dx:RibbonDropDownButtonItem Name="Zoom300" Text="300%"></dx:RibbonDropDownButtonItem>
                                <dx:RibbonDropDownButtonItem Name="Zoom400" Text="400%"></dx:RibbonDropDownButtonItem>
                                <dx:RibbonDropDownButtonItem Name="Zoom500" Text="500%"></dx:RibbonDropDownButtonItem>
                            </Items>
                        </dx:RibbonDropDownButtonItem>
                    </Items>
                </dx:RibbonGroup>
            </Groups>
        </dx:RibbonTab>
    </Tabs>
</dx:ASPxRibbon>
<div id="viewerPanel" class="viewerPanel" runat="server">
    <div>
        <dx:ASPxCallbackPanel ID="cbpViewer" runat="server" ClientInstanceName="cbpViewer" OnCallback="cbpViewer_Callback">
            <PanelCollection>
                <dx:PanelContent>
                    <dx:ASPxLabel ID="lbErrorMessage" runat="server" ForeColor="Red">
                    </dx:ASPxLabel>
                    <dx:ASPxDataView ID="ASPxDataViewDocument" runat="server">
                        <SettingsTableLayout ColumnCount="1" RowsPerPage="1" />
                        <PagerSettings Visible="false">
                        </PagerSettings>
                        <ItemTemplate>
                            <dx:ASPxBinaryImage ID="ASPxBinaryImagePDFPage" runat="server" OnDataBinding="ASPxBinaryImagePDFPage_DataBinding">
                            </dx:ASPxBinaryImage>
                        </ItemTemplate>
                        <ItemStyle>
                            <Paddings Padding="0px" />
                        </ItemStyle>
                    </dx:ASPxDataView>
                </dx:PanelContent>
            </PanelCollection>
            <ClientSideEvents EndCallback="cbpViewer_EndCallback" />
        </dx:ASPxCallbackPanel>
    </div>

</div>
<dx:ASPxPopupControl ID="ASPxPopupControlEmail" runat="server" ClientInstanceName="ASPxPopupControlEmail" HeaderText="Email" ShowHeader="true"
    PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ShowFooter="true">
    <CloseButtonStyle BackColor="Transparent" Border-BorderStyle="None" Border-BorderWidth="0" Border-BorderColor="Transparent"></CloseButtonStyle>
    <HeaderStyle CssClass="PopupHeader" BackColor="#003399" ForeColor="White" Font-Bold="true" />

    <ContentCollection>
        <dx:PopupControlContentControl>
            <dx:ASPxLabel ID="ASPxLabelEmailAddresses" runat="server" Text="Provide an email address below to send this document to."></dx:ASPxLabel>
            <dx:ASPxTextBox ID="ASPxTextBoxEmailAddresses" runat="server" Width="500px"></dx:ASPxTextBox>
            <dx:ASPxLabel ID="ASPxLabelEmailAddressNote" runat="server" Text='NOTE : To send to multiple email address, please seperate them by semicolon ";".'></dx:ASPxLabel>
            <br />
            <br />
            <dx:ASPxLabel ID="ASPxLabelSubject" runat="server" Text="Provide a subject to this email."></dx:ASPxLabel>
            <dx:ASPxTextBox ID="ASPxTextBoxSubject" runat="server" Width="500px"></dx:ASPxTextBox>
            <br />
            <dx:ASPxLabel ID="ASPxLabelContent" runat="server" Text="If needed, you can personalize the content of your email below." EncodeHtml="false"></dx:ASPxLabel>
            <dx:ASPxHtmlEditor ID="ASPxHtmlEditorContent" runat="server" Width="500px" Height="350px"></dx:ASPxHtmlEditor>
        </dx:PopupControlContentControl>

    </ContentCollection>
    <FooterContentTemplate>
        <dx:ASPxButton ID="ASPxButtonSendEmail" runat="server" Text="Send PDF to Email" BackColor="DarkRed" ForeColor="White" EnableTheming="false" Height="30px"
            AutoPostBack="false">
            <ClientSideEvents Click="ASPxButtonSendEmail_Click" />
            <Paddings Padding="5px" />
        </dx:ASPxButton>
    </FooterContentTemplate>
</dx:ASPxPopupControl>
</div>