﻿using DevExpress.Pdf;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web.UI;


public delegate string SendEmailEventHandler(object o, SendEmailEventArgs e);

public partial class PDFViewer_Page_WebUserControl : System.Web.UI.UserControl
{
    PdfDocumentProcessor _documentProcessor;

    private byte[] _pdfData;
    private int baseEdgeLength = 900;

    public event SendEmailEventHandler SendEmailClick;

    private MyEmail _email;
    public PDFViewer_Page_WebUserControl()
    {
        _documentProcessor = new PdfDocumentProcessor();
    }

   

    protected PdfDocumentProcessor DocumentProcessor
    {
        get
        {
            return _documentProcessor;
        }
    }

    public byte[] PdfData
    {
        get
        {
            return _pdfData;
        }
        set
        {
            try
            {
                _pdfData = value;
                if (value != null)
                {
                    using (MemoryStream stream = new MemoryStream(value))
                    {
                        DocumentProcessor.LoadDocument(stream, true);
                        BindDataView();
                        DisplayPdfWindow();
                    }
                }
                else ShowError("File data not available.");
            }
            catch (Exception ex)
            {
                ShowError(String.Format("File Loading Failed: {0}", ex.Message));
            }
        }
    }

    public MyEmail Email
    {
        get
        {
            _email.RecipientAddresses = ASPxLabelEmailAddresses.Text;
            _email.Subject = ASPxTextBoxSubject.Text;
            _email.Content = ASPxHtmlEditorContent.Html;
            return _email;
        }
        set
        {
            _email = value;
            if (!IsPostBack)
            {
                ASPxTextBoxEmailAddresses.Text = value.RecipientAddresses;
                ASPxTextBoxSubject.Text = value.Subject;
                ASPxHtmlEditorContent.Html = value.Content;
            }
        }
    }

    public int CurrentEgdeLength
    {
        get
        {
            if (Session["CurrentEdgeLength"] == null)
                return baseEdgeLength;
            return (int)Session["CurrentEdgeLength"];
        }
        set
        {
            Session["CurrentEdgeLength"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsCallback)
            CurrentEgdeLength = 900;
    }

    protected void ASPxBinaryImagePDFPage_DataBinding(object sender, EventArgs e)
    {
        SetCurrentPDFPage();
    }

    protected void SetCurrentPDFPage()
    {
        int pageNumber = GetCurrentPageNumber();

        ASPxBinaryImage image = ASPxDataViewDocument.FindItemControl("ASPxBinaryImagePDFPage", ASPxDataViewDocument.Items[0]) as ASPxBinaryImage;
        using (Bitmap bitmap = DocumentProcessor.CreateBitmap(pageNumber, CurrentEgdeLength))
        {
            using (MemoryStream stream = new MemoryStream())
            {
                bitmap.Save(stream, ImageFormat.Png);
                image.ContentBytes = stream.ToArray();
            }
        }
    }

    private int GetCurrentPageNumber()
    {
        var pagesTemplate = ASPxRibbonDocument.Tabs.FindByName("Main").Groups.FindByName("Navigation").Items.FindByName("Pages") as RibbonTemplateItem;
        var pagenumberSpinEdit = pagesTemplate.FindControl("ASPxSpinEditCurrentPage") as ASPxSpinEdit;
        if (pagenumberSpinEdit != null)
        {
            if (pagenumberSpinEdit.Value == null)
                pagenumberSpinEdit.Value = 1;
            return int.Parse(pagenumberSpinEdit.Value.ToString());
        }
        else return 1;
    }

    private void SetCurrentPageNumber(int pageNumber)
    {
        var pagesTemplate = ASPxRibbonDocument.Tabs.FindByName("Main").Groups.FindByName("Navigation").Items.FindByName("Pages") as RibbonTemplateItem;
        var pagenumberSpinEdit = pagesTemplate.FindControl("ASPxSpinEditCurrentPage") as ASPxSpinEdit;
        if (pagenumberSpinEdit != null)
        {
            pagenumberSpinEdit.Value = pageNumber;
        }
    }

    private string GetFindText()
    {
        var findTemplate = ASPxRibbonDocument.Tabs.FindByName("Main").Groups.FindByName("Navigation").Items.FindByName("Find") as RibbonTemplateItem;
        var findButtonEdit = findTemplate.FindControl("SearhButtonHeader") as ASPxButtonEdit;
        if (findButtonEdit != null && findButtonEdit.Value != null)
        {
            return findButtonEdit.Value.ToString();
        }
        else return string.Empty;
    }

    protected void BindDataView()
    {
        if (DocumentProcessor.Document != null)
        {
            List<PdfPageItem> data = new List<PdfPageItem>();
            var totalPages = DocumentProcessor.Document.Pages.Count;
            for (int pageNumber = 1; pageNumber <= DocumentProcessor.Document.Pages.Count; pageNumber++)
            {
                data.Add(new PdfPageItem()
                {
                    PageNumber = pageNumber
                });
            }
            SetTotalPages(totalPages);
            ASPxDataViewDocument.DataSource = data;
            ASPxDataViewDocument.DataBind();
        }
        lbErrorMessage.Text = String.Empty;
    }

    private void SetTotalPages(int totalPages)
    {
        var pagesTemplate = ASPxRibbonDocument.Tabs.FindByName("Main").Groups.FindByName("Navigation").Items.FindByName("Pages") as RibbonTemplateItem;
        var pagenumberSpinEdit = pagesTemplate.FindControl("ASPxSpinEditCurrentPage");
        if (pagenumberSpinEdit != null)
        {
            (pagenumberSpinEdit as ASPxSpinEdit).MaxValue = totalPages;
        }

        var totalPageLabel = pagesTemplate.FindControl("ASPxLabelTotalPage");
        if (totalPageLabel != null)
        {
            (totalPageLabel as ASPxLabel).Text = totalPages.ToString();
        }
    }

    protected void ShowError(string message)
    {
        ASPxDataViewDocument.DataSource = null;
        ASPxDataViewDocument.DataBind();
        lbErrorMessage.Text = message;
    }

    protected void cbpViewer_Callback(object sender, CallbackEventArgsBase e)
    {
        switch (e.Parameter)
        {
            case "Next":
            case "Previous":
            case "Pages":
                break;
            case "ZoomIn":
                {
                    int currentLength = (int)(CurrentEgdeLength * 1.25);

                    if (currentLength > 5 * baseEdgeLength)
                        currentLength = 5 * baseEdgeLength;
                    CurrentEgdeLength = currentLength;
                }
                break;
            case "ZoomOut":
                {
                    int currentLength = (int)(CurrentEgdeLength * 0.75);

                    if (currentLength < baseEdgeLength / 10)
                        currentLength = baseEdgeLength / 10;
                    CurrentEgdeLength = currentLength;
                }
                break;
            case "Zoom10":
            case "Zoom25":
            case "Zoom50":
            case "Zoom100":
            case "Zoom150":
            case "Zoom200":
            case "Zoom300":
            case "Zoom400":
            case "Zoom500":
                {
                    string perc = e.Parameter.Substring(4);
                    CurrentEgdeLength = int.Parse(perc) * baseEdgeLength / 100;
                }
                break;
            case "Find":
                FindTextInPDF(0);
                break;
            case "Find-1":
                FindTextInPDF(-1);
                break;
            case "Find+1":
                FindTextInPDF(+1);
                break;
            case "Print":
                {
                    cbpViewer.JSProperties["cp_PDFData"] = PdfData;
                    return;
                }
            case "Email":
                cbpViewer.JSProperties["cp_MailStatus"] = OnSendEmailClick(Email);
                break;
        }
        SetCurrentPDFPage();
        cbpViewer.JSProperties["cp_PageNumber"] = GetCurrentPageNumber();
    }

    private bool FindTextInPDF(int where)
    {
        string findText = GetFindText();
        if (!string.IsNullOrWhiteSpace(findText))
        {
            int resultPage = 0;
            PdfTextSearchResults result;
            List<int> pageNumbers = new List<int>();
            while ((result = DocumentProcessor.FindText(findText)).Status == PdfTextSearchStatus.Found)
            {
                HighlightResult(DocumentProcessor, result);
                if (!pageNumbers.Contains(result.PageNumber))
                    pageNumbers.Add(result.PageNumber);
            }
            if (pageNumbers.Count() > 0)
            {
                var currentPage = GetCurrentPageNumber();
                if (where == -1)
                {
                    resultPage = pageNumbers.LastOrDefault(p => p < currentPage);
                }
                else if (where == 0)
                {
                    if (!pageNumbers.Contains(currentPage))
                    {
                        resultPage = pageNumbers.FirstOrDefault(p => p > currentPage);
                        if (resultPage == 0)
                            resultPage = pageNumbers.FirstOrDefault();
                    }
                }
                else
                {
                    resultPage = pageNumbers.FirstOrDefault(p => p > currentPage);
                    if (resultPage == 0 && !pageNumbers.Contains(currentPage))
                    {
                        resultPage = pageNumbers.FirstOrDefault();
                    }
                }
            }

            if (resultPage != 0)
            {
                SetCurrentPageNumber(resultPage);
                return true;
            }
        }
        return false;
    }

    public virtual string OnSendEmailClick(MyEmail myEmail)
    {
        if (SendEmailClick != null)
        {
            return SendEmailClick.Invoke(this, new SendEmailEventArgs() { Email = myEmail });
        }

        return null;
    }

    protected class PdfPageItem
    {
        public int PageNumber
        {
            get;
            set;
        }
    }

    protected void ASPxButtonDownload_Click(object sender, EventArgs e)
    {
        Response.ContentType = "application/pdf";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + Email.Attachment + ".pdf");

        // Write the file to the Response  
        const int bufferLength = 10000;
        byte[] buffer = new Byte[bufferLength];
        int length = 0;
        Stream download = null;
        try
        {
            download = new MemoryStream(PdfData);
            do
            {
                if (Response.IsClientConnected)
                {
                    length = download.Read(buffer, 0, bufferLength);
                    Response.OutputStream.Write(buffer, 0, length);
                    buffer = new Byte[bufferLength];
                }
                else
                {
                    length = -1;
                }
            }
            while (length > 0);
            Response.Flush();
            Response.End();
        }
        finally
        {
            if (download != null)
                download.Close();
        }
    }

    public void HighlightResult(PdfDocumentProcessor processor, PdfTextSearchResults result)
    {
        for (int i = 0; i < result.Rectangles.Count; i++)
        {
            PdfTextMarkupAnnotationData annotation =
            processor.AddTextMarkupAnnotation(result.PageNumber, result.Rectangles[i], PdfTextMarkupAnnotationType.Highlight);
            if (annotation != null)
            {
                annotation.Color = new PdfRGBColor(1, 1, 0.5);
            }
        }
    }
    public void DisplayPdfWindow()
    {
        pdfWindow.Style.Add("display", "block");
    }
}

public class SendEmailEventArgs : EventArgs
{
    public MyEmail Email { get; set; }
}

public class MyEmail
{
    public string SenderAddress { get; set; }
    public string RecipientAddresses { get; set; }
    public string Subject { get; set; }
    public string Content { get; set; }
    public string Attachment { get; set; }
}
