﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CurrentTaskToPdf.aspx.cs" Inherits="CurrentTaskToPdf" %>
<%@ Register Src="~/PDFViewer_Page/WebUserControl.ascx" TagPrefix="uc" TagName="WebUserControl"  %> 
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .container {
            display: flex;
            flex-direction: column;
            justify-content:center;
            align-items: center;
            margin-bottom: 20px;
        }
        .btn {
            margin-top: 10px;
        }
    </style>
    <script src="scripts/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        function OnInit() {
            $("#divModalback3").show();
            $("#blockScreen").show();
        }
        
        function hideLoader() {
            $("#divModalback3").hide();
            $("#blockScreen").hide();
        }
    </script>
</head>
<body>    
    <form id="form1" runat="server">
            <div id="divModalback3"  style=" display:none;z-index: 0; height: 100%; width: 100%; top: 0; bottom: 0; left: 0; position: fixed; background-color: #000000; opacity: 0.4;  z-index: 9000;" runat="server"></div>
            <div id="blockScreen"  style=" display:none;padding: 4px; position: absolute; top: 35%; left: 45%; z-index: 9999;  position: fixed;   background-color: #e3e6ea; " runat="server">
                    <img src="wwwroot/gif/Loader4.gif" width="150" height="110" />
            </div>
        <div class="container">
            <dx:ASPxTextBox ID="ASPxTextBox" runat="server" Width="200px"  NullText="Enter Resource ID"></dx:ASPxTextBox> <br />
            <dx:ASPxButton CssClass="btn" ID="ASPxButtonDownload"  runat="server" Text="Generate Pdf" OnClick="btn_click" UseSubmitBehavior="true" AutoPostBack="false">
                <ClientSideEvents Click="OnInit" />  
            </dx:ASPxButton>
        </div>
        <uc:WebUserControl ID="ucWebUserControl" runat="server" />
    </form>
</body>
</html>
