Please follow the steps 
1) Clone the repository on your local machine.
2) Open Sql Script in SSMS from TaskToPdf > sqlscript> UpdatedScript.sql
3) Execute the stored procedure in your new database named as "TaskToPdf"
4) Open Web.Config and update your SSMS server name inside <connectionstring> on label "Data Source"
5) Run the code and see the output