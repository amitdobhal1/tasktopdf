USE [TaskToPdf]
GO
/****** Object:  StoredProcedure [dbo].[GetCurrentTasks]    Script Date: 02-09-2021 22:15:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER Procedure [dbo].[GetCurrentTasks]
	@ResourceID int
As
Begin

	With TaskName As
	(
		Select Distinct TaskName, TaskDesc From [dbo].[twd_task_names] 
	),
	TaskCount As
	(
		Select TaskName, Count(1) As [TaskCount]
		From [dbo].[tblCurrentTasks]
		Where ResourceID = @ResourceID
		Group By TaskName		
	),
	CurrentTask As
	(
		Select t.*, 
			c.TaskCount,
			Row_Number() Over(Partition By c.TaskName Order By c.TaskName) As CountNumber,
			Case 
				When Row_Number() Over(Partition By c.TaskName Order By c.TaskName) < (c.TaskCount / 3) Then 0
				When Row_Number() Over(Partition By c.TaskName Order By c.TaskName) < ((c.TaskCount / 3) * 2) Then 1
				Else 2
			End As OrderByUrgency,
			Case 
				When Row_Number() Over(Partition By c.TaskName Order By c.TaskName) < (c.TaskCount / 3) Then 'Critical Work to be Completed Urgently'
				When Row_Number() Over(Partition By c.TaskName Order By c.TaskName) < ((c.TaskCount / 3) * 2) Then 'Work to be Completed NOW - Please work from TOP of list'
				Else 'Work NOT ready to start yet - Please check all Purchase Orders Received'
			End As UrgencyDescription
		From [dbo].[tblCurrentTasks] t 
			Inner Join TaskCount c On c.TaskName = t.TaskName
		Where ResourceID = @ResourceID
	)


	Select s.snam 
		, s.scont
		, s.sphon
		, t.OrderByUrgency
		, t.UrgencyDescription
		, t.TaskName + ' ' + n.taskdesc As TaskName
		, t.Filter + 1 As Tasfla1925
		, t.JobPriority
		, t.DaysOnList
		, IsNull(dbo.CountWorkingDays(o.Po_assg, GetDate()), 0) As DaysAssigned
		, o.ponum
		, Case 
			When o.Po_assg Is Null Then 0
			When dbo.CountWorkingDays(o.Po_assg, GetDate()) > t.DaysOnList Then t.DaysOnList
			Else dbo.CountWorkingDays(o.Po_assg, GetDate())
		  End As DaysAllocated
		, [dbo].[CountWorkingDays](t.BaselineStart, t.BaselineFinish) As BaselineDuration
		, t.BuilderName
		, t.ClientName
		, t.LotAddress
		, Case 
			When j.coyent_id = 101 Then 'South East Queensland'
			When j.coyent_id = 102 Then 'Gold Coast'
			When j.coyent_id = 103 Then 'Coffs Harbour'
			Else 'Outside Queensland'
		  End As Region
		, t.[Power]
		, t.BookedFor
		, t.BookedOn
		--, o.PONum
		--, t.ID As t_ID, n.taskname As n_taskname, j.jobs_id As j_jobs_id, s.ReplID as s_replID, o.bordh_id
	From CurrentTask t
		Inner Join [TaskName] n On Rtrim(n.TaskName) = Rtrim(t.TaskName)
		Inner Join [dbo].[twd_jobs] j On j.Jobs_ID = t.Jobs_ID
		Inner Join [dbo].[twd_supcont] s On s.resc_id = t.ResourceID
		Left Join [dbo].[twd_bldordhd] o On o.SupplierID = s.supc_id And o.jobs_id = j.jobs_id
	Order By s.snam
			, t.OrderByUrgency
			, t.TaskName
			, j.jobs_id;

	Return @@RowCount;
End

